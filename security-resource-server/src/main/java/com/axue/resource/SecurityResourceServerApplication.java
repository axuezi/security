package com.axue.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityResourceServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityResourceServerApplication.class, args);
	}

}
